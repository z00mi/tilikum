package org.software.z00mi.tilikum;

import java.io.File;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import org.software.z00mi.tilikum.configuration.Configuration;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * Unit test for simple App.
 */
public class AppTest 
    extends TestCase
{
    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public AppTest( String testName )
    {
        super( testName );
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite()
    {
        return new TestSuite( AppTest.class );
    }

    /**
     * Rigourous Test :-)
     */
    public void testApp()
    {

        Marshaller marshaller;
		try {
			JAXBContext jc = JAXBContext.newInstance(Configuration.class);
	        Unmarshaller unmarshaller = jc.createUnmarshaller();
	        File xml = new File("C:\\Projects\\Tilikum\\tilikum\\resources\\config\\config.xml");
	        Configuration transition = (Configuration) unmarshaller.unmarshal(xml);
	        
			marshaller = jc.createMarshaller();
			marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
	        marshaller.marshal(transition, System.out);			
		} catch (JAXBException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}        
    }
}

package org.software.z00mi.tilikum.template.generator;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.software.z00mi.tilikum.configuration.TableDefinition;
import org.software.z00mi.tilikum.db.ast.infrastructure.DatabaseTable;

import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import freemarker.template.TemplateExceptionHandler;

public class TableTemplateProcessor {

	private TableDefinition definition = null; 
	private DatabaseTable table = null;
	private String templateRoot = "";
	private String connectionName;	
	
	public TableTemplateProcessor(TableDefinition td, DatabaseTable dbt, String templateRootDir, String connectionName) {
		
		this.definition = td;
		this.table = dbt;
		this.templateRoot = templateRootDir;
		this.connectionName = connectionName;
	}

	public void process() throws IOException, TemplateException {
		
		validate();		
		String outputFileName = prepareOnDiskStructure();
//		System.out.println(outputFileName);		
		
		for(String templateFileName: definition.getTemplateDef().getFileList().getTemplateFiles()){
			
			Configuration cfg = new Configuration(Configuration.VERSION_2_3_25);
	        cfg.setDirectoryForTemplateLoading(new File(templateRoot));
	        cfg.setDefaultEncoding("Cp1250");
	        cfg.setTemplateExceptionHandler(TemplateExceptionHandler.RETHROW_HANDLER);
	        cfg.setLogTemplateExceptions(false);
		
			Map<String, Object> templateVars = new HashMap<String, Object>();
			templateVars.put("Table", table);
			templateVars.put("tableGuid", UUID.randomUUID().toString().toUpperCase());
			Template tmp = cfg.getTemplate(templateFileName);
			OutputStream outStream = new FileOutputStream(outputFileName);
			Writer out = new OutputStreamWriter(outStream);				
	        tmp.process(templateVars, out);
		}
	}

	private String prepareOnDiskStructure() throws IOException {
		
		String outputFolderName = expandVariables(definition.getOutPutRoot());
		String outPutPath = expandVariables(definition.getTemplateDef().getOutput());
	
		File dir = new File(outputFolderName);
		if(!dir.exists()){			
			if(!dir.mkdirs()){
				throw new IOException("Output directory doesn't exists and cannot be created!");
			}
		}
		
		File tableDirOrFile = new File(outputFolderName, outPutPath);
		if(!tableDirOrFile.exists()){
			
			File pathToNewFile = new File(tableDirOrFile.getParentFile().getPath());
			if(!pathToNewFile.exists()){
				if(!pathToNewFile.mkdirs()){
					throw new IOException("Output directory doesn't exists and cannot be created!");
				}
			}
			
		}
		
		return tableDirOrFile.getAbsolutePath();
	}

	private String expandVariables(String text) {
		String result =  text.replaceAll("\\$\\{connectionName\\}", this.connectionName);
		result = result.replaceAll("\\$\\{tableName\\}", table.getName());
		return result;
	}

	private void validate() throws IOException {
		
		if(templateRoot.isEmpty()){
			throw new IOException("Tamplate root is empty ");
		} else {
			File f = new File(templateRoot);
			if(!f.exists() || !f.isDirectory()){
				throw new IOException("Template root have to exists and be a directory "+templateRoot);
			}
		}		
				
		for(String fileName: definition.getTemplateDef().getFileList().getTemplateFiles()){
			File f = new File(templateRoot,fileName);	

			if(!f.exists() || f.isDirectory()){
				throw new IOException("Template file doesn't exists "+fileName);
			}
		}
		
		
		
	}

	

}

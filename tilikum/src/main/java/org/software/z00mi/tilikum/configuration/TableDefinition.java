package org.software.z00mi.tilikum.configuration;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;

public class TableDefinition {

	private String tableName;
	private TableTemplateDefiniotion templateDef;
	private String outputRootFolder;
	
	public String getTableName() {
		return tableName;
	}
	
	@XmlAttribute(name = "name")
	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

	public TableTemplateDefiniotion getTemplateDef() {
		return templateDef;
	}

	@XmlElement(name="template")
	public void setTemplateDef(TableTemplateDefiniotion templateDef) {
		this.templateDef = templateDef;
	}

	public String getOutPutRoot() {		
		return this.outputRootFolder;
	}
	
	@XmlAttribute(name = "outPutRoot")
	public void setOutPutRoot(String value){
		this.outputRootFolder = value;
	}
}

package org.software.z00mi.tilikum.db.ast;

import org.software.z00mi.tilikum.db.ast.infrastructure.DatabaseObject;
import org.software.z00mi.tilikum.db.ast.infrastructure.DatabaseObjectKind;

public abstract class DbObject implements DatabaseObject {
	
	private String name;
	private DatabaseObjectKind kind;
	private String description;

	public DbObject(DatabaseObjectKind kind, String name, String description) {
		
		this.setName(name);
		this.setKind(kind);
		this.setDescription(description);
		
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public DatabaseObjectKind getKind() {
		return kind;
	}

	public void setKind(DatabaseObjectKind kind) {
		this.kind = kind;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}

package org.software.z00mi.tilikum.instrastructure;

public interface ValidationRule<T extends Object> {

	Boolean isValid(T aModel);
	String getErrorMessage();
	
}

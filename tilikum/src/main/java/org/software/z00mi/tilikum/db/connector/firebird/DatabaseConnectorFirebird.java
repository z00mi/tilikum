package org.software.z00mi.tilikum.db.connector.firebird;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Optional;
import java.util.Properties;

import org.software.z00mi.tilikum.db.ast.DbMetadata;
import org.software.z00mi.tilikum.db.ast.table.DbTable;
import org.software.z00mi.tilikum.db.ast.table.DbTableColumn;
import org.software.z00mi.tilikum.db.ast.infrastructure.DatabaseDataTypeKind;
import org.software.z00mi.tilikum.db.ast.infrastructure.DatabaseMetadata;
import org.software.z00mi.tilikum.db.ast.infrastructure.DatabaseTable;
import org.software.z00mi.tilikum.db.connector.DatabaseConnectionConfiguration;
import org.software.z00mi.tilikum.db.connector.DatabaseConnectionStringBuilder;
import org.software.z00mi.tilikum.db.connector.DatabaseConnectorAbstract;

public class DatabaseConnectorFirebird extends DatabaseConnectorAbstract {
	
	private Connection databaseConnection = null;
	
	public DatabaseConnectorFirebird(DatabaseConnectionConfiguration conn) {
		
		try {
			Class.forName("org.firebirdsql.jdbc.FBDriver");
		} catch (ClassNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
    	
		Properties params = new Properties();
    	params.setProperty("lc_ctype", "WIN1250");
    	params.setProperty("user", conn.getUserName());
    	params.setProperty("password", conn.getUserPassword());
    	
		try {
			databaseConnection = DriverManager.getConnection(
					DatabaseConnectionStringBuilder.BuildConnectionString("firebirdsql", conn), 
					params
			);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private String getTableListQuery(String specificTable){
		
			//todo refaqrwactor this
			String result = "SELECT DISTINCT TRIM(r.RDB$RELATION_NAME) AS table_name, TRIM(r.RDB$FIELD_NAME) AS field_name, r.RDB$DESCRIPTION AS field_description, "
					+ "r.RDB$DEFAULT_VALUE AS field_default_value, r.RDB$NULL_FLAG AS field_not_null_constraint, f.RDB$FIELD_LENGTH AS field_length, "
					+ "f.RDB$FIELD_PRECISION AS field_precision, f.RDB$FIELD_SCALE AS field_scale, "
					+ "CASE f.RDB$FIELD_TYPE "
					+ "WHEN 261 THEN 'BLOB' "
					+ "WHEN 14 THEN 'CHAR' "
					+ "WHEN 40 THEN 'CSTRING' "
					+ "WHEN 11 THEN 'D_FLOAT' "
					+ "WHEN 27 THEN 'DOUBLE' "
					+ "WHEN 10 THEN 'FLOAT' "
					+ "WHEN 16 THEN "
					+ "CASE f.RDB$FIELD_SUB_TYPE "
					+ "WHEN 1 THEN 'NUMERIC' "
					+ "WHEN 2 THEN 'DECIMAL' "
					+ "ELSE 'INT64' "
					+ "END "
					+ "WHEN 8 THEN 'INTEGER' "
					+ "WHEN 9 THEN 'QUAD' "
					+ "WHEN 7 THEN 'SMALLINT' "
					+ "WHEN 12 THEN 'DATE' "
					+ "WHEN 13 THEN 'TIME'"
					+ "WHEN 35 THEN 'TIMESTAMP' "
					+ "WHEN 37 THEN 'VARCHAR' "
					+ "ELSE 'UNKNOWN' "
					+ "END AS field_type_name, "
					+ "f.RDB$FIELD_TYPE AS field_type_id, "
					+ "f.RDB$FIELD_SUB_TYPE AS field_subtype "
					+ "FROM RDB$RELATION_FIELDS r "
					+ "left JOIN RDB$FIELDS f ON r.RDB$FIELD_SOURCE = f.RDB$FIELD_NAME "
					+ " WHERE r.rdb$system_flag <> 1 ";
					if ((specificTable != null) && (!specificTable.isEmpty())){
						result = result + " and r.RDB$RELATION_NAME = '" + specificTable + "'";
					}					
					result = result + "ORDER BY r.RDB$RELATION_NAME, r.RDB$FIELD_POSITION";
					
					return result;
				
	}
	@Override
	public DatabaseMetadata getMetadata() throws SQLException {
		
		if (databaseConnection.isClosed()){
			return null;
		}
		
		DbMetadata result = new DbMetadata();
		
		Statement stmt;
		try {
			stmt = databaseConnection.createStatement();
			ResultSet resultSet = stmt.executeQuery(getTableListQuery(""));
			
			
			while(resultSet.next()){
				Optional<DatabaseTable> exists = result.findTable(resultSet.getString("TABLE_NAME"));
				DbTable table;
				if(!exists.isPresent()){					
					table = new DbTable(resultSet.getString("TABLE_NAME"),"");
					result.addTable(table);
				} else {
					table = (DbTable) exists.get();
				}
				table.add(
						new DbTableColumn(
								resultSet.getString("FIELD_NAME"),
								"", 
								resultSet.getString("FIELD_TYPE_NAME"),
								mapDbTypeToDataTypeKind(
										resultSet.getInt("FIELD_TYPE_ID"),
										resultSet.getInt("FIELD_SUBTYPE")
										
								)
						)
				);
				
			}
			resultSet.close();
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return result;
	}
	
	private DatabaseDataTypeKind mapDbTypeToDataTypeKind(int TypeId, int SubTypeId) {
		// TODO Auto-generated catch block
		return DatabaseDataTypeKind.String;
	}

}

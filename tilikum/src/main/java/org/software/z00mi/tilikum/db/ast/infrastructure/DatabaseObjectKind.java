package org.software.z00mi.tilikum.db.ast.infrastructure;

public enum DatabaseObjectKind {
	Column,
	Table,
	View, 
	Procedure, 
	Constraint,
	Trigger,
	Sequence,
	Function
}

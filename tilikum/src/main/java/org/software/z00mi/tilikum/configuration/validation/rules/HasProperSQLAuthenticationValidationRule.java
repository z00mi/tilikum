package org.software.z00mi.tilikum.configuration.validation.rules;

import org.software.z00mi.tilikum.configuration.Connection;
import org.software.z00mi.tilikum.instrastructure.ValidationRule;

public class HasProperSQLAuthenticationValidationRule implements ValidationRule<Connection>{

	private String message;
	
	public Boolean isValid(Connection aModel) {
		
		if(aModel.getSqlUserName().isEmpty() || aModel.getSqlUserPassword().isEmpty()){
			message = "userName and password must be set to establish connection "+aModel.getConnectionName()+".";
			return false;
		}
		return true;
	}

	public String getErrorMessage() {
		
		return this.message;
	}

	

}

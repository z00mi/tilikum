package org.software.z00mi.tilikum.db.ast.infrastructure;

import java.util.List;
import java.util.Optional;

public interface DatabaseMetadata {

	List<DatabaseTable> getTables();
	public Optional<DatabaseTable> findTable(final String name);
}

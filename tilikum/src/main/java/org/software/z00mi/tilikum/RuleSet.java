package org.software.z00mi.tilikum;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.software.z00mi.tilikum.instrastructure.ValidationRule;
import org.software.z00mi.tilikum.instrastructure.ValidationRules;

public class RuleSet<T> implements ValidationRules<T>{

	private List<ValidationRule<T>> rules = new ArrayList<ValidationRule<T>>();
	
	public Iterator<ValidationRule<T>> iterator() {		
		return this.rules.iterator();
	}

	public int RegisterRule(ValidationRule<T> rule) {
		this.rules.add(rule);
		return 0;
	}

	public int UnregisterRule(ValidationRule<T> rule) {
		this.rules.remove(rule);
		return 0;
	}

}

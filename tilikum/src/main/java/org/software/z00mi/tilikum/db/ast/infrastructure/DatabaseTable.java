package org.software.z00mi.tilikum.db.ast.infrastructure;

import java.util.List;

public interface DatabaseTable extends DatabaseObject{
	
	List<? extends DatabaseTableColumn> getColumns();
	
}

package org.software.z00mi.tilikum.db.ast.infrastructure;

public interface DatabaseTableColumn extends DatabaseObject {

	String getDataType();
	DatabaseDataTypeKind getTypeKind();
}

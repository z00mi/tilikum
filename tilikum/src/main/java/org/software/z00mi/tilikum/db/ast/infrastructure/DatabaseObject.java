package org.software.z00mi.tilikum.db.ast.infrastructure;

public interface DatabaseObject {

	String getName();
	String getDescription();
	DatabaseObjectKind getKind();
	
}

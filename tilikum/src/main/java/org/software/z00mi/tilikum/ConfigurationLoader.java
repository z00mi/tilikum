package org.software.z00mi.tilikum;

import java.io.File;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.software.z00mi.tilikum.configuration.Configuration;
import org.software.z00mi.tilikum.configuration.Connection;
import org.software.z00mi.tilikum.configuration.validation.rules.HasDatabasePropertySetValidationRule;
import org.software.z00mi.tilikum.configuration.validation.rules.HasHostPropertySetValidationRule;
import org.software.z00mi.tilikum.configuration.validation.rules.HasProperSQLAuthenticationValidationRule;
import org.software.z00mi.tilikum.instrastructure.Validator;


public class ConfigurationLoader {

	private static final Logger log = Logger.getLogger(ConfigurationLoader.class.getName());
	
	public static final Configuration load(String fileName) throws JAXBException{
		
		JAXBContext jc = JAXBContext.newInstance(Configuration.class);
        Unmarshaller unmarshaller = jc.createUnmarshaller();
        File xml = new File(fileName);
        Configuration transition = (Configuration) unmarshaller.unmarshal(xml);
        
        validateConfiguration(transition);
		
        return transition;
	}

	private static void validateConfiguration(Configuration configurationModel) {
		
		RuleSet<Connection> ruleSet = new RuleSet<Connection>();
		ruleSet.RegisterRule(new HasHostPropertySetValidationRule());
		ruleSet.RegisterRule(new HasDatabasePropertySetValidationRule());
		ruleSet.RegisterRule(new HasProperSQLAuthenticationValidationRule());
		
		Validator<Connection> v = configurationModel.getValidator(); 
		if(!v.isValidFor(ruleSet)){
			for(String message: v.getValidationErrorMessages()){
				log.log(Level.INFO, message);
			}
		}
	}
}

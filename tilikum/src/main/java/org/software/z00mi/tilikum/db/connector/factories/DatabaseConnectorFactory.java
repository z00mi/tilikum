package org.software.z00mi.tilikum.db.connector.factories;

import org.software.z00mi.tilikum.db.connector.DatabaseConnectionConfiguration;
import org.software.z00mi.tilikum.db.connector.DatabaseConnector;
import org.software.z00mi.tilikum.db.connector.firebird.DatabaseConnectorFirebird;

public class DatabaseConnectorFactory {

	public static DatabaseConnector getConnector(DatabaseConnectionConfiguration conn) {

		if(conn.getJdbcDriverName().equals("firebirdsql")){
			return new DatabaseConnectorFirebird(conn);
		}
		return null;
	}

}

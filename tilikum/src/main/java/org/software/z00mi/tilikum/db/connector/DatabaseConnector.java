package org.software.z00mi.tilikum.db.connector;

import java.sql.SQLException;

import org.software.z00mi.tilikum.db.ast.infrastructure.DatabaseMetadata;

public interface DatabaseConnector {	

	public DatabaseMetadata getMetadata() throws SQLException;

}

package org.software.z00mi.tilikum.db.ast;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.software.z00mi.tilikum.db.ast.infrastructure.DatabaseMetadata;
import org.software.z00mi.tilikum.db.ast.infrastructure.DatabaseTable;

public class DbMetadata implements DatabaseMetadata {

	private ArrayList<DatabaseTable> tables;
	
	public DbMetadata() {
		
		tables = new ArrayList<DatabaseTable>();
	}
	
	public List<DatabaseTable> getTables() {
		
		return this.tables;
	}

	public Boolean addTable(DatabaseTable table){
		return this.tables.add(table);
	}
	
	public Boolean removeTable(DatabaseTable table){
		return this.tables.remove(table);
	}
	
	public Optional<DatabaseTable> findTable(final String name){
	    return tables.stream().filter(o -> o.getName().equals(name)).findFirst();
	}
}

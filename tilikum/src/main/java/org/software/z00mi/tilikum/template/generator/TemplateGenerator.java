package org.software.z00mi.tilikum.template.generator;

import java.io.IOException;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.software.z00mi.tilikum.configuration.Connection;
import org.software.z00mi.tilikum.configuration.TableDefinition;
import org.software.z00mi.tilikum.db.ast.infrastructure.DatabaseMetadata;
import org.software.z00mi.tilikum.db.ast.infrastructure.DatabaseTable;

import freemarker.template.TemplateException;
import freemarker.template.utility.NullArgumentException;

public class TemplateGenerator {
	
	private Connection configuration;
	private DatabaseMetadata metadata;
	private static final Logger log = Logger.getLogger(TemplateGenerator.class.getName());
	
	public TemplateGenerator(Connection configuration, DatabaseMetadata metaData) {
		
		if(configuration == null){
			throw new NullArgumentException("configuration");
		}
		if(metaData == null){
			throw new NullArgumentException("metdata");
		}
		this.configuration = configuration;
		this.metadata = metaData;
	}

	public void run() {
		
		
		for(String ex: configuration.getExcludedObjects().getExclusions()){
			Optional<DatabaseTable> t = metadata.findTable(ex);
			if(t.isPresent()){				
				log.log(Level.INFO, "Table " + t.get().getName()+" has been removed from tables to process list.");
				metadata.getTables().remove(t.get());
			}			
		}
		
		for(DatabaseTable dbt: metadata.getTables()){	
			
			for(TableDefinition td: configuration.getDefiniotions()){
				
				if(dbt.getName().matches(td.getTableName())){
					
					log.log(Level.INFO, "Generating template for "+dbt.getName()+" cuase of getName().matches(\""+td.getTableName()+"\");");
					
					TableTemplateProcessor processor = new TableTemplateProcessor(td, dbt, configuration.getTemplateRootDir(), configuration.getConnectionName());
					try {
						processor.process();
					} catch (IOException | TemplateException e) {
						log.log(Level.WARNING, "Template generation for table "+dbt.getName()+" skipped due to incomplete or errornous definition");
						log.log(Level.SEVERE, e.getMessage());
					}
				}
			}
						
		}
		
	}

}

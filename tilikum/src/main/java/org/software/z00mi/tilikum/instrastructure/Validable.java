package org.software.z00mi.tilikum.instrastructure;

public interface Validable<T> {

	Validator<T> getValidator();	
	
}

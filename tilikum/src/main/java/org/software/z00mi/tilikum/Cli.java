package org.software.z00mi.tilikum;

import java.io.File;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;

public class Cli {

	private static final Logger log = Logger.getLogger(Cli.class.getName());
	private String[] args = null;
    private Options cmdOptions = new Options();
    private Properties options = null;
    
	public Cli(String[] args) {
		
		this.args = args;
		
		cmdOptions.addOption("c", "config", true, "configuration file name.");
		cmdOptions.addOption("t", "threads", false, "start separate thread for each connection defined in config");
		cmdOptions.addOption("h", "help", false, "show help.");
		
		this.options = new Properties();
	}

	public void parse(){
		
		CommandLineParser parser = new DefaultParser();
		CommandLine cmd = null;
		
		try {
			cmd = parser.parse(cmdOptions, args);
			if (cmd.hasOption("h")){
				help();
			}
			if (cmd.hasOption("c")) {						
				File f = new File(cmd.getOptionValue("c"));
				if (f.exists() && !f.isDirectory()) {
					log.log(Level.INFO, "Using cli argument -c=" + cmd.getOptionValue("c"));
					options.setProperty("configFileName", f.getAbsolutePath());					
				} else {
					log.log(Level.SEVERE, "File doesn't exists - " + cmd.getOptionValue("c"));					
				}
			} else {		
				log.log(Level.SEVERE, "Missing config option");		
				help();		
			}
		} catch (ParseException e) {
			log.log(Level.SEVERE, "Failed to parse comand line properties", e);
			help();		
		}
	}

	private void help() {
		HelpFormatter formater = new HelpFormatter();
		formater.printHelp("Main", cmdOptions);
		System.exit(0);		
	}
		
	public String getConfigurationFileName(){
		return options.getProperty("configFileName");
	}
	
	
}

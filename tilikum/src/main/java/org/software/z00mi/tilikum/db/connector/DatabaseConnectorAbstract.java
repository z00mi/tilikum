package org.software.z00mi.tilikum.db.connector;

import java.sql.SQLException;

import org.software.z00mi.tilikum.db.ast.infrastructure.DatabaseMetadata;

public abstract class DatabaseConnectorAbstract implements DatabaseConnector{
		
	public abstract DatabaseMetadata getMetadata() throws SQLException;

}

package org.software.z00mi.tilikum.db.ast.table;

import java.util.ArrayList;
import java.util.List;

import org.software.z00mi.tilikum.db.ast.DbObject;
import org.software.z00mi.tilikum.db.ast.infrastructure.DatabaseObjectKind;
import org.software.z00mi.tilikum.db.ast.infrastructure.DatabaseTable;
import org.software.z00mi.tilikum.db.ast.infrastructure.DatabaseTableColumn;

public class DbTable extends DbObject implements DatabaseTable{

	private ArrayList<DatabaseTableColumn> columns;

	public DbTable(String name, String description) {
		super(DatabaseObjectKind.Table, name, description);
		
		this.columns = new ArrayList<DatabaseTableColumn>(); 
	}

	public List<DatabaseTableColumn> getColumns() {

		return this.columns;
		
	}

	public Boolean add(DatabaseTableColumn column){
		return columns.add(column);
	}
	
	public Boolean remove(DatabaseTableColumn column){
		return columns.remove(column);
	}
}

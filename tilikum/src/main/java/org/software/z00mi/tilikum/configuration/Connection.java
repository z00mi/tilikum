package org.software.z00mi.tilikum.configuration;

import java.util.List;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;

import org.software.z00mi.tilikum.db.connector.DatabaseConnectionConfiguration;

public class Connection implements DatabaseConnectionConfiguration{
	
	private String remoteHost;
	private int remotePort;
	private String sqlUserName;
	private String sqlUserPassword;
	private String databaseName;
	private String jdbcDriverName;
	private Exclusions excludedObjects;
	private String connectionName;
	private String templateRootDir;
	 
	private boolean disabled = false;
	
	private List<TableDefinition> definiotions;
		
	public String getRemoteHost() {
		return remoteHost;
	}
	
	@XmlAttribute(name = "host")
	public void setRemoteHost(String remoteHost) {
		this.remoteHost = remoteHost;
	}
	
	public int getRemotePort() {
		return remotePort;
	}
	
	@XmlAttribute(name = "port")
	public void setRemotePort(int remotePort) {
		this.remotePort = remotePort;
	}
	
	public String getSqlUserName() {
		return sqlUserName;
	}
	
	@XmlAttribute(name = "userName")
	public void setSqlUserName(String sqlUserName) {
		this.sqlUserName = sqlUserName;
	}
	
	public String getSqlUserPassword() {
		return sqlUserPassword;
	}
	
	@XmlAttribute(name = "password")
	public void setSqlUserPassword(String sqlUserPassword) {
		this.sqlUserPassword = sqlUserPassword;
	}
	
	public String getDatabaseName() {
		return databaseName;
	}
	
	@XmlAttribute(name = "database")
	public void setDatabaseName(String databaseName) {
		this.databaseName = databaseName;
	}

	public String getJdbcDriverName() {
		return jdbcDriverName;
	}

	@XmlAttribute(name = "driver")
	public void setJdbcDriverName(String jdbcDriverName) {
		this.jdbcDriverName = jdbcDriverName;
	}

	public Exclusions getExcludedObjects() {
		if(excludedObjects == null){
			excludedObjects = new Exclusions();
		}
		return excludedObjects;
	}

	@XmlElement(name = "exclusions")
	public void setExcludedObjects(Exclusions excludedObjects) {
		this.excludedObjects = excludedObjects;
	}

	public List<TableDefinition> getDefiniotions() {
		return definiotions;
	}

	@XmlElement(name = "table")
	public void setDefiniotions(List<TableDefinition> definiotions) {
		this.definiotions = definiotions;
	}

	public String getConnectionName() {
		return connectionName;
	}
	
	@XmlAttribute(name = "name")
	public void setConnectionName(String connectionName) {
		this.connectionName = connectionName;
	}

	public String getTemplateRootDir() {
		return templateRootDir;
	}

	@XmlAttribute(name = "templateRoot")
	public void setTemplateRootDir(String templateRootDir) {
		this.templateRootDir = templateRootDir;
	}

	public void disableForGenerationProcess() {
		
		this.disabled  = true;
		
	}

	public boolean isDisabled() {
		
		return disabled;
	}

	public String getHost() {		
		return remoteHost;
	}

	public int getPort() {
		return remotePort;
	}

	public String getUserName() {
		return sqlUserName;
	}

	public String getUserPassword() {
		return sqlUserPassword;
	}
	
}

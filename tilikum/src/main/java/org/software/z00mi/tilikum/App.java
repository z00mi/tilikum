package org.software.z00mi.tilikum;

import java.sql.SQLException;

import javax.xml.bind.JAXBException;

import org.software.z00mi.tilikum.configuration.Configuration;
import org.software.z00mi.tilikum.configuration.Connection;
import org.software.z00mi.tilikum.db.ast.infrastructure.DatabaseMetadata;
import org.software.z00mi.tilikum.db.ast.infrastructure.DatabaseTable;
import org.software.z00mi.tilikum.db.ast.infrastructure.DatabaseTableColumn;
import org.software.z00mi.tilikum.db.connector.DatabaseConnector;
import org.software.z00mi.tilikum.db.connector.factories.DatabaseConnectorFactory;
import org.software.z00mi.tilikum.template.generator.TemplateGenerator;

public class App 
{
    public static void main( String[] args )
    {
        Cli c = new Cli(args);
        c.parse();
        
        try {
			Configuration config = ConfigurationLoader.load(c.getConfigurationFileName());
			
			for(Connection conn: config.getConnections()){
				
				if(!conn.isDisabled() && !conn.getDefiniotions().isEmpty()){
					
					DatabaseConnector connector = DatabaseConnectorFactory.getConnector(conn);
					DatabaseMetadata metaData = connector.getMetadata();
//					for(DatabaseTable t: metaData.getTables()){
//						System.out.println(t.getName());
//						for(DatabaseTableColumn col: t.getColumns()){
//							System.out.println("  "+col.getName()+": "+col.getDataType());
//						}
//					}
					TemplateGenerator generator = new TemplateGenerator(conn, metaData);
					generator.run();
						
				}
					
			}
		} catch (JAXBException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
}

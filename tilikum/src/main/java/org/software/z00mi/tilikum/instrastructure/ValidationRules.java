package org.software.z00mi.tilikum.instrastructure;

public interface ValidationRules<T extends Object> extends Iterable<ValidationRule<T>>{

	int RegisterRule(ValidationRule<T> rule);
	int UnregisterRule(ValidationRule<T> rule);
	
}

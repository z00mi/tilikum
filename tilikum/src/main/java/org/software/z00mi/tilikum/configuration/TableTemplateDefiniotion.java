package org.software.z00mi.tilikum.configuration;

import javax.xml.bind.annotation.XmlElement;

public class TableTemplateDefiniotion {

	private TemplateFileList fileList;	
	private String output;
	private String outPutRootFolder;
	
	public TemplateFileList getFileList() {
		return fileList;
	}

	@XmlElement(name = "files")
	public void setFileList(TemplateFileList fileList) {
		this.fileList = fileList;
	}
	
	public String getOutput() {
		return output;
	}
	
	@XmlElement(name = "outPutPath")
	public void setOutput(String output) {
		this.output = output;
	}

	public String getOutPutRootFolder() {
		return outPutRootFolder;
	}
	
	@XmlElement(name = "outPutRoot")
	public void setOutPutRootFolder(String outPutRootFolder) {
		this.outPutRootFolder = outPutRootFolder;
	}
}

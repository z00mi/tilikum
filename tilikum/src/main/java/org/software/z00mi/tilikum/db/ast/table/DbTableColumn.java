package org.software.z00mi.tilikum.db.ast.table;

import org.software.z00mi.tilikum.db.ast.DbObject;
import org.software.z00mi.tilikum.db.ast.infrastructure.DatabaseDataTypeKind;
import org.software.z00mi.tilikum.db.ast.infrastructure.DatabaseObjectKind;
import org.software.z00mi.tilikum.db.ast.infrastructure.DatabaseTableColumn;

public class DbTableColumn extends DbObject implements DatabaseTableColumn{

	private String dataType;
	private DatabaseDataTypeKind typeKind;

	public DbTableColumn(String name, String description, String dataType, DatabaseDataTypeKind typeKind) {
		super(DatabaseObjectKind.Column, name, description);

		this.setDataType(dataType);
		this.setTypeKind(typeKind);
	}

	public String getDataType() {
		return this.dataType;
	}

	public void setDataType(String dataType) {
		this.dataType = dataType;
	}
	
	public DatabaseDataTypeKind getTypeKind() {

		return this.typeKind;
	}

	public void setTypeKind(DatabaseDataTypeKind typeKind) {
		this.typeKind = typeKind;
	}

}

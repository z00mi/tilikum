package org.software.z00mi.tilikum.configuration.validation.rules;

import org.software.z00mi.tilikum.configuration.Connection;
import org.software.z00mi.tilikum.instrastructure.ValidationRule;

public class HasHostPropertySetValidationRule implements ValidationRule<Connection>{

	private String message;
	
	public Boolean isValid(Connection aModel) {
		
		if(aModel.getRemoteHost().isEmpty()){
			message = "No host information provided for connection object "+aModel.getConnectionName()+".";
			return false;
		}
		return true;
	}

	public String getErrorMessage() {
		
		return this.message;
	}

	

}

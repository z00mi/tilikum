package org.software.z00mi.tilikum.configuration.validation.rules;

import org.software.z00mi.tilikum.configuration.Connection;
import org.software.z00mi.tilikum.instrastructure.ValidationRule;

public class HasDatabasePropertySetValidationRule implements ValidationRule<Connection>{

	private String message;
	
	public Boolean isValid(Connection aModel) {
		
		if(aModel.getDatabaseName().isEmpty()){
			message = "No database information provided for connection object "+aModel.getConnectionName()+".";
			return false;
		}
		return true;
	}

	public String getErrorMessage() {
		
		return this.message;
	}

	

}

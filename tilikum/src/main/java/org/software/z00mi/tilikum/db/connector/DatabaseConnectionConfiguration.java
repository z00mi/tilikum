package org.software.z00mi.tilikum.db.connector;

public interface DatabaseConnectionConfiguration {
	
	String getHost();
	int getPort();
	String getUserName();
	String getUserPassword();
	String getDatabaseName();
	String getJdbcDriverName();
//	Properties getSpecificParams();
}

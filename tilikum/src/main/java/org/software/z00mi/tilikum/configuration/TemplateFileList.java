package org.software.z00mi.tilikum.configuration;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;

public class TemplateFileList {
	
	private List<String> templateFiles;

	public List<String> getTemplateFiles() {
		return templateFiles;
	}

	@XmlElement(name = "file")
	public void setTemplateFiles(List<String> templateFiles) {
		this.templateFiles = templateFiles;
	}
	
	
}

package org.software.z00mi.tilikum.db.ast.infrastructure;

public enum DatabaseDataTypeKind {
	
	String,
	Integer,
	Numeric,
	Float,
	Boolean,
	Binary, 
	Text,
	Date,
	Time,
	TimeStamp,
	GUID
	
}

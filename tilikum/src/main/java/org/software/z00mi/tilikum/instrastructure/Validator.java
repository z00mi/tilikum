package org.software.z00mi.tilikum.instrastructure;

import java.util.List;

public interface Validator<T> {
	
	Boolean isValidFor(ValidationRules<T> ruleSet);
	List<String> getValidationErrorMessages();
}

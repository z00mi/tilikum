package org.software.z00mi.tilikum.configuration;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;

public class Exclusions {

	private List<String> exclusions;

	public List<String> getExclusions() {
		if(this.exclusions == null){
			this.exclusions = new ArrayList<String>();
		}
		return exclusions;
	}
	
	@XmlElement(name = "exlcude")
	public void setExclusions(List<String> exclusions) {
		this.exclusions = exclusions;
	}
}

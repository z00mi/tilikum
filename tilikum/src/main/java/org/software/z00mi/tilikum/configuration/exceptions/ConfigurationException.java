package org.software.z00mi.tilikum.configuration.exceptions;

public class ConfigurationException extends Exception{

	private static final long serialVersionUID = 5817253819238935550L;

	public ConfigurationException(String message){ 
		super(message); 
	}
}

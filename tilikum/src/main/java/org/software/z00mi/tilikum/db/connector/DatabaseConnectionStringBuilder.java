package org.software.z00mi.tilikum.db.connector;

public class DatabaseConnectionStringBuilder {

	public static String BuildConnectionString(String jdbcDriverName, DatabaseConnectionConfiguration configuration){
		
		String result = "jdbc:";
		
		result = result + jdbcDriverName +":" + buildDatabasePath(configuration);
		
		return result;
		
	}


	private static String buildDatabasePath(DatabaseConnectionConfiguration configuration) {
		
		String result = configuration.getHost();
		
		if (!result.isEmpty()) {
			if (configuration.getPort() != 0){
				result = result + "/" + Integer.toString(configuration.getPort());
			}
			result = result + ":";
		}
		result = result + configuration.getDatabaseName();
		return result;
	}

}

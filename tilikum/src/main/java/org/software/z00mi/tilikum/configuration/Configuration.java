package org.software.z00mi.tilikum.configuration;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.*;

import org.software.z00mi.tilikum.instrastructure.Validable;
import org.software.z00mi.tilikum.instrastructure.ValidationRule;
import org.software.z00mi.tilikum.instrastructure.ValidationRules;
import org.software.z00mi.tilikum.instrastructure.Validator;

@XmlRootElement(name = "configuration")
public class Configuration implements Validable<Connection> {
		
	private List<Connection> connections;	
	
	public List<Connection> getConnections() {
		return connections;
	}
	
	@XmlElement(name="connection")
	public void setConnections(List<Connection> connections) {
		this.connections = connections;
	}

	private 
	class ConnectionValidator implements Validator<Connection>{

		private List<Connection> cons;
		private List<String> errorMessages = new ArrayList<String>();
		
		public ConnectionValidator(List<Connection> connections) {

			this.cons = connections;
		}

		public Boolean isValidFor(ValidationRules<Connection> ruleSet) {
		
			for(Connection c: this.cons){
								
				for(ValidationRule<Connection> r: ruleSet){
					
					if(!r.isValid(c)){
						this.errorMessages.add(r.getErrorMessage());
						c.disableForGenerationProcess();			
					}
				}
				if(c.isDisabled()) {
					this.errorMessages.add("Due to validation errors connection "+c.getConnectionName()+" will be disabled for generation process.");
				}
			}
			
			return this.errorMessages.isEmpty();
		}

		public List<String> getValidationErrorMessages() {
			return this.errorMessages;
		}
		
	}

	public Validator<Connection> getValidator() {
		
		return new ConnectionValidator(this.connections);
	}

	
	
	
	
	 
}
